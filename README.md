# DinaBot

This project is a bot in python. This bot is used to play the Dinasaur game. (http://www.trex-game.skipser.com)

This project has been done with **Pycharm** IDE and three packages :
*  PyAutoGUI
*  numpy
*  Pillow

To run correctly the project, you'll need to put the IDE window on the right 
side of your screen, and the game on the left side.

![Sans_titre](/uploads/6bae1d3b7bdc755ccc91327ba81e792d/Sans_titre.png)

The system is very simple. Before running the programm, change values in the 
class line 6. You need to change the values of replayBtn and dinosaur. These
values are coordinates on your screen.

`class Cordinates():
    replayBtn = (780, 640)
    dinosaur = (450, 805)`
    
ReplayBtn variable is used to localise the replay button of the game so that the 
bot can start the game himself.
The dinosaur variable is used to indicate the area where the bot needs to check 
if there's an obstacle to jump.